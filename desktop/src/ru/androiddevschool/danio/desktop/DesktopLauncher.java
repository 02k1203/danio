package ru.androiddevschool.danio.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ru.androiddevschool.danio.DanIO;

public class DesktopLauncher {
	public static void main (String[] arg) {
		System.setProperty("user.name", "EnglishWords");
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 640;
		config.height = 360;
		new LwjglApplication(DanIO.get(), config);
	}
}
