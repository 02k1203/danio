package ru.androiddevschool.danio.Controller;

import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;

import ru.androiddevschool.danio.Model.Player;

/**
 * Created by 02k1203 on 20.04.2017.
 */
public class PlayerController extends Touchpad {
    Player player;

    public PlayerController(float deadzoneRadius, TouchpadStyle style, Player player) {
        super(deadzoneRadius, style);
        this.player = player;
    }
    public void setPlayer(Player player){
        this.player = player;
    }

    public void act(float delta) {
        super.act(delta);
        player.setRotation((float) Math.toDegrees(Math.atan2(getKnobPercentY(), getKnobPercentX())));
        player.setSpeedMult(Math.sqrt(Math.pow(getKnobPercentX(), 2) + Math.pow(getKnobPercentY(), 2)) > 0 ? 1 : 0);
    }
}
