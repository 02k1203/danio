package ru.androiddevschool.danio.Screens;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Timer;

import java.util.Random;

import ru.androiddevschool.danio.Controller.PlayerController;
import ru.androiddevschool.danio.Model.Food;
import ru.androiddevschool.danio.Model.Mob;
import ru.androiddevschool.danio.Model.Player;
import ru.androiddevschool.danio.Model.World;
import ru.androiddevschool.std.Screens.StdScreen;
import ru.androiddevschool.std.Utils.Values;

/**
 * Created by 02k1203 on 13.04.2017.
 */
public class Play extends StdScreen {
    private static Random r = new Random();
    private World world;
    private Player player;
    private Timer timer;
    PlayerController controller;

    public Play() {
        super();
    }

    public void show(){
        super.show();
        world.refresh();
        player = new Player(assets.imgs.get("spheres/sphere-02"), 30);
        world.addPlayer(player);
        controller.setPlayer(player);
        timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                Food food = new Food(assets.imgs.get("spheres/sphere-00"),player.getSize());
                food.setPosition(player.getX() + r.nextInt()%(Values.WORLD_WIDTH/2), player.getY() + r.nextInt()%(Values.WORLD_HEIGHT/2));
                world.addFood(food);
            }
        }, 5, 2);
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                Mob mob = new Mob(assets.imgs.get("spheres/sphere-01"), (int) (player.getSize() + Values.FOOD * (r.nextInt()%2)));
                mob.setPosition(player.getX() + r.nextInt()%(Values.WORLD_WIDTH/2), player.getY() + r.nextInt()%(Values.WORLD_HEIGHT/2));
                mob.setTarget(player);
                world.addMob(mob);
            }
        }, 2,1);
        timer.start();
        assets.music.get("m1").play();
    }
    public void hide(){
        super.hide();
        timer.stop();
        assets.music.get("m1").stop();
    }

    @Override
    protected void initBg(Stage stage) {
        Image image = new Image(assets.imgs.get("bg/sky"));
        image.setSize(Values.WORLD_WIDTH,Values.WORLD_HEIGHT);
        stage.addActor(image);
    }

    @Override
    protected void initWorld(Stage stage) {
        world = new World(stage.getViewport(), stage.getBatch(), assets);
        stages.put(StageType.WORLD, world);

    }
    protected void postAct() {
        world.getCamera().position.x = player.getX();
        world.getCamera().position.y = player.getY();
        ((OrthographicCamera)(world.getCamera())).zoom = 1.0f*player.getSize()/40;
    }

    @Override
    protected void initUi(Stage stage) {
        controller = new PlayerController(0, assets.touchpadStyles.get("normal"), player);
        controller.setPosition(50, 50);
        stage.addActor(controller);

        Button button= new TextButton("Clone",(TextButton.TextButtonStyle) assets.btnStyles.get("menu"));
        button.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y){
                world.removePlayer();
                player = new Player((TextureRegionDrawable) player.getDrawable(), player.getSize()/2);
                world.addPlayer(player);
                controller.setPlayer(player);

            }
        });
        button.setPosition(Values.WORLD_WIDTH - button.getWidth(), 0);
        stage.addActor(button);
    }
}