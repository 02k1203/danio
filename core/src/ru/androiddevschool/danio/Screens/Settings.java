package ru.androiddevschool.danio.Screens;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.sun.prism.paint.Stop;

import ru.androiddevschool.std.Screens.StdScreen;

/**
 * Created by 02k1203 on 13.04.2017.
 */
public class Settings extends StdScreen {
    public Settings() {
        super();
    }
    @Override
    protected void initBg(Stage stage) {

    }

    @Override
    protected void initWorld(Stage stage) {

    }

    @Override
    protected void initUi(Stage stage) {
        Button button= new TextButton("Stop music",(TextButton.TextButtonStyle) assets.btnStyles.get("menu"));
        button.addListener(assets.music.(Stop))
    }
}