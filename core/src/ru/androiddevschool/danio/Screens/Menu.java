package ru.androiddevschool.danio.Screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import ru.androiddevschool.std.Controller.ScreenTraveler;
import ru.androiddevschool.std.Screens.StdScreen;
import ru.androiddevschool.std.Utils.Values;

import static ru.androiddevschool.std.Utils.Names.ScreenName.*;


/**
 * Created by 02k1203 on 06.04.2017.
 */
public class Menu extends StdScreen {
    public Menu() {
        super();
    }

    @Override
    protected void initBg(Stage stage) {
        Image image = new Image(assets.imgs.get("bg/soil"));
        image.setSize(Values.WORLD_WIDTH, Values.WORLD_HEIGHT);
        stage.addActor(image);
    }

    @Override
    protected void initWorld(Stage stage) {

    }

    @Override
    protected void initUi(Stage stage) {
        Button button;
        Label label; // текстовый актер
        Image image; //картинковый актер
        Table layout = new Table();
        layout.setFillParent(true);
        layout.setDebug(true);


        if (assets == null) System.out.println("Assets");
        if (assets.btnStyles == null) System.out.println("btnstyles");
        button = new TextButton("Играть", (TextButton.TextButtonStyle) assets.btnStyles.get("menu"));
        button.addListener(new ScreenTraveler(PLAY));
        layout.add(button).row();

        button = new TextButton("Настройки", (TextButton.TextButtonStyle) assets.btnStyles.get("menu"));
        button.addListener(new ScreenTraveler(SETTINGS));
        layout.add(button).row();

        stage.addActor(layout);
    }

    public void show(){
        super.show();
        assets.music.get("m0").play();

    }
    public void hide(){
        super.hide();
        assets.music.get("m0").stop();

    }
}
