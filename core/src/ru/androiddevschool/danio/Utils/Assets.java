package ru.androiddevschool.danio.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.HashMap;

import ru.androiddevschool.std.Utils.StdAssets;

/**
 * Created by ga_nesterchuk on 06.04.2017.
 */
public class Assets extends StdAssets{
    public Assets() {
        super();
        initSfx();
        initImages();
        initFonts();
        initBtnStyles();
        initTouchStyles();
    }

    private void initSfx() {
        music = new HashMap<String, Music>();
        for( int i = 0; i < 5; i++)
            music.put("m"+i, Gdx.audio.newMusic(getHandle("audio/music/m"+i+".mp3")));
        sounds = new HashMap<String, Sound>();
        for( int i = 0; i < 1; i++)
            sounds.put("s"+i, Gdx.audio.newSound(getHandle("audio/sounds/s"+i+".mp3")));
    }

    private void initTouchStyles() {
        touchpadStyles = new HashMap<String, Touchpad.TouchpadStyle>();
        touchpadStyles.put("normal",
                new Touchpad.TouchpadStyle(
                        imgs.get("touch/bg-normal"),
                        imgs.get("touch/knob-normal")
                ));
    }

    private void initBtnStyles() {
        btnStyles = new HashMap<String, Button.ButtonStyle>();
        btnStyles.put("menu", new TextButton.TextButtonStyle(
                        imgs.get("ui/btn-up"),
                        imgs.get("ui/btn-down"),
                        null,
                        fonts.get("menu")
                )
        );
        btnStyles.put("small-menu", new Button.ButtonStyle(
                        imgs.get("ui/btn-up-small"),
                        imgs.get("ui/btn-down-small"),
                        null
                )
        );

    }

    private void initFonts() {
        fonts = new HashMap<String, BitmapFont>();
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(getHandle("fonts/sandy.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.characters = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюяABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ";
        parameter.color = new Color(0x21ff0aff);
        parameter.size = 90;
        fonts.put("menu", generator.generateFont(parameter));
    }

    private void initImages() {
        imgs = new HashMap<String, TextureRegionDrawable>();
        //System.out.println("init");
        addFolderImg(getHandle("images/ui/"), "");
        addFolderImg(getHandle("images/bg/"), "");
        addFolderImg(getHandle("images/touch/"), "");
        addFolderImg(getHandle("images/spheres/"), "");
        for (String name : imgs.keySet()) System.out.println(name);
        //addFolderImg(getHandle(""), "");
    }

    private void addFolderImg(FileHandle file, String prefix) {
        //System.out.println(file.path());
        if (file.isDirectory())
            for (FileHandle f : file.list())
                addFolderImg(f, prefix + file.name());
        else if (file.extension().equals("png") || file.extension().equals("jpg"))
            imgs.put(prefix + "/" + file.nameWithoutExtension(), makeDrawable(file));
    }

    private TextureRegionDrawable makeDrawable(FileHandle handle) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(handle)));
    }

    private FileHandle getHandle(String fileName) {
        return Gdx.files.internal(fileName);
    }

    public HashMap<String, TextureRegionDrawable> imgs;
    public HashMap<String, Button.ButtonStyle> btnStyles;
    public HashMap<String, BitmapFont> fonts;
    public HashMap<String, Touchpad.TouchpadStyle> touchpadStyles;
    public HashMap<String, Music> music;
    public HashMap<String, Sound> sounds;
}