package ru.androiddevschool.danio;

import ru.androiddevschool.danio.Screens.Menu;
import ru.androiddevschool.danio.Screens.Play;
import ru.androiddevschool.danio.Screens.Settings;
import ru.androiddevschool.danio.Utils.Assets;
import ru.androiddevschool.std.StdGame;
import ru.androiddevschool.std.Utils.Global;

import static ru.androiddevschool.std.Utils.Names.ScreenName.MENU;
import static ru.androiddevschool.std.Utils.Names.ScreenName.PLAY;
import static ru.androiddevschool.std.Utils.Names.ScreenName.SETTINGS;

public class DanIO extends StdGame{
	private static DanIO instance = new DanIO();
	public static DanIO get() {return instance;}
	private DanIO(){}

	private Assets assets;
	@Override
	public void postLoad() {
		initGlobals();
		this.assets = new Assets();
		addScreen(MENU, new Menu());
		addScreen(PLAY, new Play());
		addScreen(SETTINGS, new Settings());
		setScreen(MENU);
	}

	@Override
	public void initGlobals() {
		Global.assets = new Assets();
	}
}