package ru.androiddevschool.danio.Model;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by 02k1203 on 20.04.2017.
 */
public class Mob extends Char {
    private Actor target;
    private int speed;
    public boolean ate;

    public Mob(TextureRegionDrawable img, int size) {
        super(img, size);
        this.speed = 10 * size;
        this.ate = false;
    }

    public void eat(){
        if (!ate){
            super.eat();
            ate = true;
        }
    }

    public void act(float delta) {
        super.act(delta);
        if (target != null) setRotation((float) Math.toDegrees(Math.atan2(target.getY() - getY(), target.getX() - getX())));

    }

    public Actor getTarget() {
        return target;
    }

    public void setTarget(Actor target) {
        this.target = target;
        setSpeedMult(1);
    }
}
