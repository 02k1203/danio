package ru.androiddevschool.danio.Model;

import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import ru.androiddevschool.std.Utils.Values;

/**
 * Created by 02k1203 on 20.04.2017.
 */
public class Player extends Char {

    public Player(TextureRegionDrawable img, int size) {
        super(img, size);
        speed = Values.MOB_SPEED*3/2;
    }
}
