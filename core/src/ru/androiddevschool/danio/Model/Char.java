package ru.androiddevschool.danio.Model;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import ru.androiddevschool.std.Utils.Global;
import ru.androiddevschool.std.Utils.Values;


/**
 * Created by 02k1203 on 20.04.2017.
 */
public class Char extends Image {
    private int size;
    public float speed;
    private float speedMult;

    public Char(TextureRegionDrawable img, int size) {
        super(img);
        this.size = size;
        setSize(size, size);
        setOrigin(Align.center);
        this.speed = Values.MOB_SPEED;
        this.speedMult = 0;
    }

    public void eat() {
        size += Values.FOOD;
        setSize(size, size);
        setOrigin(Align.center);
        this.speedMult = 10f/size;
        Global.assets.sounds.get("s0").play();
    }

    public void die() {
        remove();
    }

    public void act(float delta) {
        super.act(delta);
        moveBy((float) (speedMult * speed * Math.cos(Math.toRadians(getRotation()))), (float) (speedMult * speed * Math.sin(Math.toRadians(getRotation()))));
    }

    public void setSpeedMult(int speedMult) {
        this.speedMult = speedMult;
    }

    public boolean canEat(Char chr) {
        return (this.size > chr.size);
    }

    public int getSize() {
        return size;
    }
}
