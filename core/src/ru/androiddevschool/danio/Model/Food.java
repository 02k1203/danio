package ru.androiddevschool.danio.Model;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

import ru.androiddevschool.danio.Utils.Assets;

/**
 * Created by Даня on 23.04.2017.
 */
public class Food extends Char {
    public Food(TextureRegionDrawable img, int size) {
        super(img, size);
    }
}
