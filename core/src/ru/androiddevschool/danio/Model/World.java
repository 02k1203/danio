package ru.androiddevschool.danio.Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.HashSet;

import ru.androiddevschool.danio.DanIO;
import ru.androiddevschool.danio.Utils.Assets;
import ru.androiddevschool.std.Utils.Names;

/**
 * Created by 02k1203 on 20.04.2017.
 */
public class World extends Stage {
    Player player;
    HashSet<Char> foods;
    HashSet<Char> mobs;
    HashSet<Char> toDelete;

    public World(Viewport screenViewport, Batch batch, Assets assets) {
        super(screenViewport, batch);
        refresh();
    }

    public void refresh() {
        foods = new HashSet<Char>();
        mobs = new HashSet<Char>();
        toDelete = new HashSet<Char>();
    }


    public void addPlayer(Player player) {
        this.player = player;
        addActor(player);
        for(Char mob : mobs){
            ((Mob)mob).setTarget(player);
        }
    }

    public void addFood(Food food) {
        foods.add(food);
        addActor(food);
    }

    public void addMob(Mob mob) {
        mobs.add(mob);
        addActor(mob);
    }

    public void act(float delta) {
        super.act(delta);
        for (Char mob : mobs) {
            if (dist(mob.getX(), mob.getY(), player.getX(), player.getY()) < player.getSize() + mob.getSize()) {
                if (mob.getSize() > player.getSize()) {
                    mob.eat();
                    if (mob.eat(){
                        hp--;
                    }
                        if (hp==0){
                            DanIO.get().setScreen(Names.ScreenName.MENU);
                    }

                }
                else {
                    player.eat();
                    toDelete.add(mob);
                }
            }
        }
        for (Char food : foods) {
            if (dist(food.getX(), food.getY(), player.getX(), player.getY()) < player.getSize() + food.getSize()) {
                player.eat();
                toDelete.add(food);
            }
        }

        for (Char ch : toDelete) {
            mobs.remove(ch);
            foods.remove(ch);
            ch.remove();
        }
        toDelete.clear();
    }

    private float dist(float x1, float y1, float x2, float y2) {
        return (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public HashSet<Char> getMobs() {
        return mobs;
    }

    public void removePlayer() {
        for (Char mob : mobs) ((Mob) mob).setTarget(null);
        player.remove();
    }
    int hp=10;

int score =summSize/mobs;
    System.out.println('your score is 'score)
int score=2*(summSize/mobs)
        int summSize=
                for(int i=0; i=mobs; i++; ) {
        mobs.size().get;
    }

 /*               алгоритм начисления очков 2*  средний размер моба *время  */

}

