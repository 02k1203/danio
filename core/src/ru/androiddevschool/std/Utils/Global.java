package ru.androiddevschool.std.Utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import ru.androiddevschool.danio.DanIO;
import ru.androiddevschool.danio.Utils.Assets;
import ru.androiddevschool.std.StdGame;

/**
 * Created by Гриша on 15.05.2017.
 */
public class Global {
    public static StdGame game = DanIO.get();
    public static SpriteBatch batch;
    public static ShapeRenderer renderer;
    public static AssetManager manager;
    public static Assets assets;
}
